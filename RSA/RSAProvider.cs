﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace RSA
{
    /// <summary>
    /// Блочное шифрование RSA
    /// </summary>
    class RSAProvider
    {
        #region fields
        BigInteger _p { get; set; }
        BigInteger _q { get; set; }
        BigInteger _n { get; set; }

        BigInteger _euler { get; set; }
        /// <summary>
        /// Открытый ключ
        /// </summary>
        BigInteger _e { get; set; }
        /// <summary>
        /// Закрытый ключ
        /// </summary>
        BigInteger _d { get; set; }
        /// <summary>
        /// Длина блока
        /// </summary>
        int _k { get; set; }
        /// <summary>
        /// Мощность алфавита
        /// </summary>
        int _N { get; set; }

        #endregion fields

        #region constructor
        public RSAProvider()
        {
            InitProvider();
        }
        #endregion constructor

        #region methods
        /// <summary>
        /// Инициализация p, q, n
        /// </summary>
        void InitProvider()
        {
            _p = BigInteger.Parse("75066426620115677532791308649310045924623341753564457704600598043980597747489");
            _q = BigInteger.Parse("80446016069342312367540014877990767693920534260591961883498229554642257583029"); 
            _n = _p * _q;
            _euler = Euler();
            _e = GenerateE();
            _d = GenerateD();
            _N = 256;
            _k = 63;
        }

        /// <summary>
        /// Функция Эйлера
        /// </summary>
        /// <returns></returns>
        BigInteger Euler()
        {
            return (_p - 1) * (_q - 1);
        }

        /// <summary>
        /// Обратное по модулю
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="d"></param>
        void ExtendedEuclid(BigInteger a, BigInteger b, out BigInteger x, out BigInteger y, out BigInteger d)
        {
            BigInteger q, r, x1, x2, y1, y2;
            if (b == 0)
            {
                d = a;
                x = 1;
                y = 0;
                return;
            }
            x2 = 1;
            x1 = 0;
            y2 = 0;
            y1 = 1;
            while (b > 0)
            {
                q = a / b;
                r = a - q * b;
                x = x2 - q * x1;
                y = y2 - q * y1;
                a = b;
                b = r;
                x2 = x1;
                x1 = x;
                y2 = y1;
                y1 = y;
            }
            d = a;
            x = x2;
            y = y2;
        }

        /// <summary>
        /// Вычисление закрытого ключа
        /// </summary>
        /// <returns></returns>
        BigInteger GenerateD()
        {
            ExtendedEuclid(_e, _euler, out BigInteger x, out BigInteger y, out BigInteger d);
            if (d != 1)
                x = 0;
            if (x < 0)
                x = _euler + x;
            return x;
        }

        /// <summary>
        /// Генерация открытого ключа
        /// </summary>
        /// <returns></returns>
        BigInteger GenerateE()
        {
            BigInteger max = _p > _q ? _p : _q;
            while (BigInteger.GreatestCommonDivisor(++max, _euler) != 1);
            return max;
        }

        /// <summary>
        /// Шифрование
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public string Encrypt(string Data)
        {
            string encryptData = "";
            //считаем, что кодировка cp1251 (так как русские символы должны быть в пределах 2 байтов)
            var data = Encoding.GetEncoding(1251).GetBytes(Data);
            for (int i = 0; i < data.Length; i = i + _k)
            {
                BigInteger block = 0;
                int j = 0;
                // формируем блок
                while (j < _k && i + j < data.Length)
                {
                    var cp1251 = data[i + j];
                    block += cp1251 * BigInteger.Pow(_N, j);
                    j++;
                }
                // шифруем блок
                var encryptBlock = BigInteger.ModPow(block, _e, _n);
                // заменяем целое число символом из cp1251
                j = _k - 1;
                string cp1251Block = "";
                while (j > -1)
                {
                    var modPow = BigInteger.ModPow(_N, j, _n);
                    block = encryptBlock / modPow;
                    encryptBlock %= modPow;
                    var value = Convert.ToInt32(block.ToString());
                    cp1251Block = (char)value + cp1251Block;
                    j--;
                }
                encryptData += cp1251Block;
            }
            return encryptData;
        }
        
        /// <summary>
        /// Дешифрование
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public string Decrypt(string data)
        {
            string decryptData = "";
            for (int i = 0; i < data.Length; i = i + _k)
            {
                // заменяем блок символов из cp1251 целыми числами
                BigInteger encryptBlock = 0;
                var j = _k - 1;
                while (j > -1 && i + j < data.Length)
                {
                    encryptBlock += data[i + j] * BigInteger.ModPow(_N, j, _n);
                    j--;
                }
                // дешифруем блок
                var decryptBlock = BigInteger.ModPow(encryptBlock, _d, _n);
                // заменяем целое число символом из cp1251
                j = _k - 1;
                string cp1251DecryptBlock = "";
                while (j > -1)
                {
                    var modPow = BigInteger.ModPow(_N, j, _n);
                    var value = decryptBlock / modPow;
                    decryptBlock -= value * modPow;
                    byte[] array = new byte[1];
                    array[0] = Convert.ToByte(value.ToString());
                    if (array[0] > 0)
                        cp1251DecryptBlock = Encoding.GetEncoding(1251).GetString(array) + cp1251DecryptBlock;
                    j--;
                }
                decryptData += cp1251DecryptBlock;
            }
            return decryptData;
        }
        #endregion methods
    }
}

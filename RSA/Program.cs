﻿using System;
using System.IO;
using System.Numerics;
using System.Text;
using System.Xml;

namespace RSA
{
    class Program
    {
        static void Main(string[] args)
        {
            var encXml = readFromFile("hello.xml");
            var rsa = new RSAProvider();
            var resultDecrypt = rsa.Decrypt(encXml);
            Console.WriteLine("Результат записан в файл");
            writeToFile(resultDecrypt, "decr.xml");
        }

        /// <summary>
        /// Считываем зашифрованный файл
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        static string readFromFile(string path)
        {
            return File.ReadAllText(path);
        }

        /// <summary>
        /// Просто запись в файл без проверок
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path"></param>
        static void writeToFile(string data, string path)
        {
            File.WriteAllText(path, data);
        }

        /// <summary>
        /// При записи XML будет проверка на теги
        /// </summary>
        /// <param name="data"></param>
        /// <param name="path"></param>
        static void writeToXml(string data, string path)
        {
            var xml = new XmlDocument();
            xml.InnerXml = data;
            xml.Save(path);
        }
    }
}
